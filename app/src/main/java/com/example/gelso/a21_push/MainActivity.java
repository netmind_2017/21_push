package com.example.gelso.a21_push;

import android.app.Notification;
import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //start code
        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //Notification
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.notification_icon)
                                .setContentTitle("YEAHHHH")
                                .setContentText("el Curso es terminado :-)) APLAUSO ! ");
                NotificationManager mNotificationManager =(NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
                mNotificationManager.notify(0, mBuilder.build());

            }

        });


    }
}
